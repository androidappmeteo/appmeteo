package com.example.isen.appmeteo.Model;

/**
 * Created by isen on 13/01/2016.
 */
public class WeatherInfo {
    private Weather weather;
    private MainInformation mainInformation;

    public WeatherInfo(Weather weather, MainInformation mainInformation) {
        this.weather = weather;
        this.mainInformation = mainInformation;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public MainInformation getMainInformation() {
        return mainInformation;
    }

    public void setMainInformation(MainInformation mainInformation) {
        this.mainInformation = mainInformation;
    }
}
