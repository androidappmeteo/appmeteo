package com.example.isen.appmeteo.Activity;

import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.isen.appmeteo.Fragments.DrawerFragment;
import com.example.isen.appmeteo.Model.FiveDayForecast;
import com.example.isen.appmeteo.Model.FunctionPreference;
import com.example.isen.appmeteo.Model.WeatherInfo;
import com.example.isen.appmeteo.R;
import com.example.isen.appmeteo.adapter.ForecastListViewAdapter;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements DrawerFragment.FragmentDrawerListener {

    private static final String API_URL_FORECAST = "http://api.openweathermap.org/data/2.5/forecast?q=";
    private static final String API_URL_CURRENT = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static String IMG_URL = "http://openweathermap.org/img/w/";
    private static final String API_KEY = "34baae032e5a26de7a10fc3cad30097e";

    public enum Request {FORECAST,CURRENT};
    FloatingActionButton addPreference;
    ProgressBar progressBar;
    TextView cityTextView;
    TextView weatherTextView;
    TextView temperatureTextView;
    ImageView weatherImageView;
    ListView forecastListView;
    private DrawerFragment drawerFragment;
    private Toolbar toolbar;

    @Override
    protected void onResume() {
        super.onResume();
        cityTextView.setText("");
        weatherImageView.setImageDrawable(null);
        weatherTextView.setText("");
        forecastListView.setAdapter(null);
    }

    FiveDayForecast fiveDayForecast;

    PlaceAutocompleteFragment autocompleteFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        progressBar = (ProgressBar) findViewById(R.id.progressBarSearch);
        cityTextView = (TextView) findViewById(R.id.citySearchTextView);
        weatherTextView = (TextView) findViewById(R.id.weatherSearchTextView);
        temperatureTextView = (TextView) findViewById(R.id.temperatureSearchTextView);
        weatherImageView = (ImageView) findViewById(R.id.weatherSearchImageView);
        forecastListView = (ListView) findViewById(R.id.forecastSearchlistview);
        addPreference = (FloatingActionButton) findViewById(R.id.addPreferencesSearch);

        toolbar = (Toolbar) findViewById(R.id.toolbarSearch);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        drawerFragment = (DrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragmentDrawerSearch);
        drawerFragment.setUp(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer_layoutSearch), toolbar);
        drawerFragment.setDrawerListener(this);


        autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                new RetrieveFeedTask(place.getName().toString(), Request.FORECAST).execute();
                autocompleteFragment.setText("Rechercher");
            }

            @Override
            public void onError(Status status) {
                Log.i("Error", "An error occurred: " + status);
            }
        });

            addPreference.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (fiveDayForecast != null && fiveDayForecast.getCity() != null) {
                        FunctionPreference.addPref(fiveDayForecast.getCity().getName().toString(), getApplicationContext());
                    }
                }
            });
        }

    private class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private final String location;
        private final Request request;

        public RetrieveFeedTask(String location, Request request){
            this.location = location.replace("","");
            this.request = request;
        }



        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            cityTextView.setText("");
            weatherTextView.setText("");
        }

        protected String doInBackground(Void... urls) {

            try {
                URL url;
                if (request == Request.CURRENT){
                    url = new URL(API_URL_CURRENT + location +"&APPID="+API_KEY);
                }else{
                    url = new URL(API_URL_FORECAST + location +"&APPID="+API_KEY);
                }
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {

            if(response == null) {
                response = "THERE WAS AN ERROR";
            }
            try {
                JSONObject object = new JSONObject(response);
                Gson gson = new Gson();

                Reader reader = new StringReader(response);

                if (object.get("cod") != "404")
                fiveDayForecast = gson.fromJson(reader, FiveDayForecast.class);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            Log.i("INFO", response);

            if (fiveDayForecast != null && fiveDayForecast.getCity() != null){
                Picasso.with(SearchActivity.this)
                        .load(IMG_URL + fiveDayForecast.getListOfWeatherForcast().get(0).getWeather().get(0).getIcon() + ".png")
                        .centerCrop().resize(150,150)
                        .into(weatherImageView);
                cityTextView.setText(fiveDayForecast.getCity().getName());
                weatherTextView.setText(fiveDayForecast.getListOfWeatherForcast().get(0).getWeather().get(0).getDescription());
                double tempK = fiveDayForecast.getListOfWeatherForcast().get(0).getMain().getTemp() - 273.15;
                int tempC = (int) tempK;
                temperatureTextView.setText(String.valueOf(tempC) + "°");

                final ArrayList<WeatherInfo> weatherInfoList = new ArrayList<WeatherInfo>();
                WeatherInfo day2 = new WeatherInfo(fiveDayForecast.getListOfWeatherForcast().get(7).getWeather().get(0),fiveDayForecast.getListOfWeatherForcast().get(7).getMain());
                WeatherInfo day3 = new WeatherInfo(fiveDayForecast.getListOfWeatherForcast().get(15).getWeather().get(0),fiveDayForecast.getListOfWeatherForcast().get(15).getMain());
                WeatherInfo day4 = new WeatherInfo(fiveDayForecast.getListOfWeatherForcast().get(23).getWeather().get(0),fiveDayForecast.getListOfWeatherForcast().get(23).getMain());
                WeatherInfo day5 = new WeatherInfo(fiveDayForecast.getListOfWeatherForcast().get(31).getWeather().get(0),fiveDayForecast.getListOfWeatherForcast().get(31).getMain());

                weatherInfoList.add(day2);
                weatherInfoList.add(day3);
                weatherInfoList.add(day4);
                weatherInfoList.add(day5);


                final ForecastListViewAdapter adapter = new ForecastListViewAdapter(getApplicationContext(), weatherInfoList);
                forecastListView.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {

    }
}


