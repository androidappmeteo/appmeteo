package com.example.isen.appmeteo.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.isen.appmeteo.Model.FunctionPreference;
import com.example.isen.appmeteo.R;

import java.util.ArrayList;
import java.util.List;


/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DrawerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DrawerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DrawerFragment extends Fragment {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private PreferenceListViewAdapter adapter;
    private View containerView;
    private static String[] titles = null;
    private FragmentDrawerListener drawerListener;
    private static DrawerFragment context;

    private ListView preferenceListView;

    public DrawerFragment(){

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public static List<String> getData() {
        List<String> data = new ArrayList<>();

        // preparing navigation drawer items
        for (int i = 0; i < FunctionPreference.indexMax(context.getContext()); i++) {
            data.add(FunctionPreference.showOnePref(i+1,context.getContext()));
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        // drawer labels
        titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_drawer, container, false);

        adapter = new PreferenceListViewAdapter(getActivity(), getData());

        preferenceListView = (ListView) layout.findViewById(R.id.drawerList);
        preferenceListView.setAdapter(adapter);
        return layout;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    private BroadcastReceiver preferencesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            adapter.setPreferenceList(getData());
            adapter.notifyDataSetChanged();
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());

        IntentFilter filter = new IntentFilter("preferences.updated");
        manager.registerReceiver(preferencesReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());

        manager.unregisterReceiver(preferencesReceiver);
    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }

    public class PreferenceListViewAdapter extends BaseAdapter {


        private List<String> mPreferenceList;
        private final Context context;

        public  PreferenceListViewAdapter(Context context, List<String> preferenceList){
            this.mPreferenceList = preferenceList;
            this.context = context;
        }
        @Override
        public int getCount() {
            return null != mPreferenceList ? mPreferenceList.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return null != mPreferenceList ? mPreferenceList.get(position) : null;
        }

        public void setPreferenceList(List<String> preferenceList) {
            this.mPreferenceList = preferenceList;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }



        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final View view = mInflater.inflate(R.layout.preference_content,null);
            final TextView nameCity = (TextView) view.findViewById(R.id.nameCity);
            final ImageButton removeOnePref=(ImageButton) view.findViewById(R.id.removePref);

            nameCity.setText(String.valueOf(getItem(position)));
            removeOnePref.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            FunctionPreference.removeOnePreference(String.valueOf(getItem(position)), context);
                            mPreferenceList.remove(getItem(position));
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            });

            return view;
        }

    }

}
