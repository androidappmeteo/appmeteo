package com.example.isen.appmeteo.Model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by isen on 09/01/2016.
 */
public class FiveDayForecast {
    @SerializedName("city")
    private City city;
    @SerializedName("cod")
    private int cod;
    @SerializedName("message")
    private double message;
    @SerializedName("cnt")
    private int count;
    @SerializedName("list")
    private List<WeatherForecast> listOfWeatherForcast;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<WeatherForecast> getListOfWeatherForcast() {
        return listOfWeatherForcast;
    }

    public void setListOfWeatherForcast(List<WeatherForecast> listOfWeatherForcast) {
        this.listOfWeatherForcast = listOfWeatherForcast;
    }
}
