package com.example.isen.appmeteo.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.isen.appmeteo.Activity.MainActivity;
import com.example.isen.appmeteo.Model.FiveDayForecast;
import com.example.isen.appmeteo.Model.FunctionPreference;
import com.example.isen.appmeteo.Model.WeatherInfo;
import com.example.isen.appmeteo.R;
import com.example.isen.appmeteo.adapter.ForecastListViewAdapter;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class WeatherFragment extends Fragment {

    private static final String API_URL_FORECAST = "http://api.openweathermap.org/data/2.5/forecast?q=";
    private static final String API_URL_CURRENT = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static String IMG_URL = "http://openweathermap.org/img/w/";
    private static final String API_KEY = "34baae032e5a26de7a10fc3cad30097e";

    public enum Request {FORECAST,CURRENT};
    ProgressBar progressBar;
    TextView cityTextView;
    TextView weatherTextView;
    TextView temperatureTextView;
    ImageView weatherImageView;
    ListView forecastListView;

    FiveDayForecast fiveDayForecast;

    public WeatherFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        int position = ((MainActivity)getActivity()).getFragmentPosition(this);

        View rootView = inflater.inflate(R.layout.fragment_weather, container, false);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        cityTextView = (TextView) rootView.findViewById(R.id.cityTextView);
        temperatureTextView = (TextView) rootView.findViewById(R.id.temperatureTextView);
        weatherTextView = (TextView) rootView.findViewById(R.id.weatherTextView);

        weatherImageView = (ImageView) rootView.findViewById(R.id.weatherImageView);
        forecastListView = (ListView) rootView.findViewById(R.id.forecast_listview);

        if (FunctionPreference.showOnePref(position + 1, this.getContext())  != null){

            new RetrieveFeedTask(FunctionPreference.showOnePref(position +1, this.getContext()), Request.FORECAST).execute();

        }

        return rootView;
    }

    private class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private final String location;
        private final Request request;

        public RetrieveFeedTask(String location, Request request){
            this.location = location.replace("","");
            this.request = request;
        }

        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            cityTextView.setText("");
            weatherTextView.setText("");
        }

        protected String doInBackground(Void... urls) {

            try {
                URL url;
                if (request == Request.CURRENT){
                    url = new URL(API_URL_CURRENT + location +"&APPID="+API_KEY);
                }else{
                    url = new URL(API_URL_FORECAST + location +"&APPID="+API_KEY);
                }
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {

            if(response == null) {
                response = "THERE WAS AN ERROR";
            }
            try {
                JSONObject object = new JSONObject(response);
                Gson gson = new Gson();

                Reader reader = new StringReader(response);

                if(object.get("cod") != "404" ) {
                    fiveDayForecast = gson.fromJson(reader, FiveDayForecast.class);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            Log.i("INFO", response);

            if (fiveDayForecast != null && fiveDayForecast.getCity() != null){
                Picasso.with(getContext())
                        .load(IMG_URL + fiveDayForecast.getListOfWeatherForcast().get(0).getWeather().get(0).getIcon() + ".png")
                        .centerCrop().resize(150,150)
                        .into(weatherImageView);
                cityTextView.setText(fiveDayForecast.getCity().getName());
                weatherTextView.setText(fiveDayForecast.getListOfWeatherForcast().get(0).getWeather().get(0).getDescription());
                double tempK = fiveDayForecast.getListOfWeatherForcast().get(0).getMain().getTemp() - 273.15;
                int tempC = (int) tempK;
                temperatureTextView.setText(String.valueOf(tempC) + "°");

                final ArrayList<WeatherInfo> weatherInfoList = new ArrayList<WeatherInfo>();
                WeatherInfo day2 = new WeatherInfo(fiveDayForecast.getListOfWeatherForcast().get(7).getWeather().get(0),fiveDayForecast.getListOfWeatherForcast().get(7).getMain());
                WeatherInfo day3 = new WeatherInfo(fiveDayForecast.getListOfWeatherForcast().get(15).getWeather().get(0),fiveDayForecast.getListOfWeatherForcast().get(15).getMain());
                WeatherInfo day4 = new WeatherInfo(fiveDayForecast.getListOfWeatherForcast().get(23).getWeather().get(0),fiveDayForecast.getListOfWeatherForcast().get(23).getMain());
                WeatherInfo day5 = new WeatherInfo(fiveDayForecast.getListOfWeatherForcast().get(31).getWeather().get(0),fiveDayForecast.getListOfWeatherForcast().get(31).getMain());

                weatherInfoList.add(day2);
                weatherInfoList.add(day3);
                weatherInfoList.add(day4);
                weatherInfoList.add(day5);


                final ForecastListViewAdapter adapter = new ForecastListViewAdapter(getContext(), weatherInfoList);
                forecastListView.setAdapter(adapter);
            }
        }
    }
}