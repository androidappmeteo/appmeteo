package com.example.isen.appmeteo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.isen.appmeteo.Model.WeatherInfo;
import com.example.isen.appmeteo.R;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by isen on 13/01/2016.
 */
public class ForecastListViewAdapter extends BaseAdapter {

    private static String IMG_URL = "http://openweathermap.org/img/w/";

    private List<WeatherInfo> mWeatherInfoList;
    private final Context context;

    public  ForecastListViewAdapter(Context context, List<WeatherInfo> weatherInfoList){
        this.mWeatherInfoList = weatherInfoList;
        this.context = context;
    }
    @Override
    public int getCount() {
        return null != mWeatherInfoList ? mWeatherInfoList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return null != mWeatherInfoList ? mWeatherInfoList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View view = mInflater.inflate(R.layout.forecast_weather_container,null);

        final TextView dayOfWeek = (TextView) view.findViewById(R.id.dayContainerTextView);
        final TextView minTemperature = (TextView) view.findViewById(R.id.minTemperatureContainerTextView);
        final ImageView weatherIcon = (ImageView) view.findViewById(R.id.iconContainerImageView);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, position + 1);
        String name = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.FRENCH);
        dayOfWeek.setText(name);

        Picasso.with(context)
                .load(IMG_URL + ((WeatherInfo) getItem(position)).getWeather().getIcon() + ".png")
                .centerCrop().resize(150,150)
                .into(weatherIcon);
        Double i = ((WeatherInfo) getItem(position)).getMainInformation().getTempMin() - 273.15;
        int i2 = i.intValue();

        minTemperature.setText(String.valueOf(i2) + "°");

        return view;
    }
}
