package com.example.isen.appmeteo.Model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by adrien on 11/01/2016.
 */
public class FunctionPreference extends Activity{

    public static void addPref(String cityToAdd,Context context)
    {
        SharedPreferences pref = context.getSharedPreferences("listCity",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        int index = indexMax(context)+1;
        boolean error=false;
        for (int tours=1;tours<index;tours++)
        {
            String cityDoesExist=pref.getString(String.valueOf(tours), "empty").toLowerCase();

            if (cityDoesExist.equals(cityToAdd.toLowerCase()))
            {
                tours=index;
                Toast.makeText(context,"the preferences does exist",Toast.LENGTH_LONG).show();
                error=true;
            }
        }
        if (error==false) {
            editor.putString(String.valueOf(index), cityToAdd).commit();
            Toast.makeText(context, "saved", Toast.LENGTH_LONG).show();

            Intent intent = new Intent();
            intent.setAction("preferences.updated");
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        }
    }
    public static String showOnePref(int key,Context context)
    {
        SharedPreferences pref = context.getSharedPreferences("listCity", context.MODE_PRIVATE);
        String city= pref.getString(String.valueOf(key), "empty");
        return city;
    }
    public static void removeOnePreference(String city,Context context)
    {

        SharedPreferences pref = context.getSharedPreferences("listCity", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        int index = indexMax(context);
        int key=0;
        for (int tours=1;tours<index;tours++)
        {
            if (showOnePref(tours,context).equals(city))
            {
             key=tours;
            }
            else
            {
             key=1;
            }
        }
        for (int tours = key; tours < index; tours++)
        {
            String cityToChange = pref.getString(String.valueOf(tours + 1), "Empty");
            editor.putString(String.valueOf(tours), cityToChange).apply();
        }
        editor.remove(String.valueOf(index)).apply();
        Toast.makeText(context,"erase this pref",Toast.LENGTH_LONG).show();

        Intent intent = new Intent();
        intent.setAction("preferences.updated");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
    public static void changeLevelOfPref(String cityChangeLevel,int newLevelPref,Context context)
    {
        SharedPreferences pref = context.getSharedPreferences("listCity", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        int index =indexMax(context);
        if (index < newLevelPref) {
            for (int tours = index; tours < newLevelPref; tours++) {
                String dataMove = pref.getString(String.valueOf(tours + 1), "empty");       //stockage de la ville à déplacer
                editor.putString(String.valueOf(tours), dataMove).apply();                  //décalage des autres villes
            }
            editor.putString(String.valueOf(newLevelPref),cityChangeLevel).apply();  //mettre la ville a l'ordre voulu
        }
        else {
            for (int tours = index; tours > newLevelPref; tours--) {
                String dataMove = pref.getString(String.valueOf(tours - 1), "empty");   //stockage de la ville à déplacer
                editor.putString(String.valueOf(tours), dataMove).apply();              //décalage des autres villes
            }
            editor.putString(String.valueOf(newLevelPref),cityChangeLevel).apply(); //mettre la ville a l'ordre voulu
        }

    }
    public static int indexMax(Context context)
    {
        SharedPreferences pref = context.getSharedPreferences("listCity", context.MODE_PRIVATE);
        int numberTotalOfPref = 1;
        while (!pref.getString(String.valueOf(numberTotalOfPref), "Empty").equals("Empty")) {
            numberTotalOfPref++;
        }
        numberTotalOfPref--;
        return numberTotalOfPref;
    }

}
