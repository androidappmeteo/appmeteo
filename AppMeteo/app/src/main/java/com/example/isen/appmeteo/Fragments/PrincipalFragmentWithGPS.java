package com.example.isen.appmeteo.Fragments;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.isen.appmeteo.Model.FiveDayForecast;
import com.example.isen.appmeteo.Model.FunctionPreference;
import com.example.isen.appmeteo.R;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class PrincipalFragmentWithGPS extends Fragment implements View.OnClickListener, LocationListener {

    private static final String API_URL_FORECAST = "http://api.openweathermap.org/data/2.5/forecast?q=";
    private static final String API_URL_CURRENT = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static String IMG_URL = "http://openweathermap.org/img/w/";
    private static final String API_KEY = "34baae032e5a26de7a10fc3cad30097e";

    public enum Request {FORECAST,CURRENT};
    FloatingActionButton addPreference;
    ProgressBar progressBar;
    TextView cityTextView;
    TextView weatherTextView;
    TextView countryTextView;
    ImageView weatherImageView;

    FiveDayForecast fiveDayForecast;

    LocationManager lManager;
    Location location;
    public String villeActuelle="";
    TextView adresse;

    public PrincipalFragmentWithGPS() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_principal_with_gps, container, false);
        addPreference = (FloatingActionButton) rootView.findViewById(R.id.addPreferences);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        cityTextView = (TextView) rootView.findViewById(R.id.cityTextView);
        weatherTextView = (TextView) rootView.findViewById(R.id.weatherTextView);
        countryTextView = (TextView) rootView.findViewById(R.id.countryTextView);
        weatherImageView = (ImageView) rootView.findViewById(R.id.weatherImageView);
        adresse =(TextView)rootView.findViewById(R.id.adresse);

        //On récupère le service de localisation

        ((TextView)rootView.findViewById(R.id.adresse)).setText("");
        rootView.findViewById(R.id.choix_source).setOnClickListener(this);

        return rootView;
    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.choix_source:
               // choisirSource();
                obtenirPosition();

                try {
                    afficherAdresse();
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this.getContext(), "votre ville est : introuvable" , Toast.LENGTH_SHORT).show();
                    adresse.setText("votre ville est introuvable");
                }
                new RetrieveFeedTask(villeActuelle, Request.FORECAST).execute();
                break;
        }
    }


    private void obtenirPosition() {
        //on démarre le cercle de chargement
        //setProgressBarIndeterminateVisibility(true);

        //On demande au service de localisation de nous notifier tout changement de position
        //sur la source (le provider) choisie, toute les minutes (60000millisecondes). changement a 600 ms
        //Le paramètre this spécifie que notre classe implémente LocationListener et recevra
        //les notifications.
        LocationManager locationManager = (LocationManager) this.getContext().getSystemService(Context.LOCATION_SERVICE);
        android.location.LocationListener locationListener = new android.location.LocationListener() {
            public void onLocationChanged(Location location) {
                //Any method here
            }
            public void onStatusChanged (String provider, int status, Bundle extras){}
            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled (String provider){}
        };
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,0,0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

    }


    private void afficherAdresse() throws IOException {
        //setProgressBarIndeterminateVisibility(true);

        //Le geocoder permet de récupérer ou chercher des adresses
        //gràce à un mot clé ou une position
        Geocoder geo = new Geocoder(this.getContext(), Locale.getDefault());
            //Ici on récupère la premiere adresse trouvé gràce à la position que l'on a récupéré
        double lat=50.633333;
        double lng=3.066667;

        List <Address> adresses =geo.getFromLocation(lat, lng, 1);
        if(adresses != null && adresses.size() > 0){
                Address adress = adresses.get(0);

            villeActuelle=adress.getLocality();
            adresse.setText("votre ville est : "+villeActuelle);
            Toast.makeText(this.getContext(), "votre ville est : " +villeActuelle, Toast.LENGTH_SHORT).show();

            }
        else{
            adresse.setText("votre ville est : introuvable");
            Toast.makeText(this.getContext(), "votre ville est : introuvable " , Toast.LENGTH_SHORT).show();

        }


        //on stop le cercle de chargement
        //setProgressBarIndeterminateVisibility(false);
    }

    public void onLocationChanged(Location location) {
        //Lorsque la position change
        //on stop le cercle de chargement
        //setProgressBarIndeterminateVisibility(false);
        //... on active le bouton pour afficher l'adresse
        //findViewById(R.id.afficherAdresse).setEnabled(true);
        //... on sauvegarde la position
        this.location = location;
        //... on l'affiche
        //afficherLocation();
        //... et on spécifie au service que l'on ne souhaite plus avoir de mise à jour
        lManager.removeUpdates((LocationListener) this);
    }

    public void onProviderDisabled(String provider) {
        //Lorsque la source (GSP ou réseau GSM) est désactivé
        //...on affiche un Toast pour le signaler à l'utilisateur
        /*Toast.makeText(PrincipalFragmentWithGPS.this,
                String.format("La source \"%s\" a été désactivé", provider),
                Toast.LENGTH_SHORT).show();*/
        //... et on spécifie au service que l'on ne souhaite plus avoir de mise à jour
        lManager.removeUpdates((LocationListener) this);
        //... on stop le cercle de chargement
        //setProgressBarIndeterminateVisibility(false);
    }

    public void onProviderEnabled(String provider) {

    }

    public void onStatusChanged(String provider, int status, Bundle extras) {

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        addPreference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fiveDayForecast != null && fiveDayForecast.getCity() != null) {
                    FunctionPreference.addPref(fiveDayForecast.getCity().getName().toString(), getContext());
                }
            }
        });
    }

    private class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private final String location;
        private final Request request;

        public RetrieveFeedTask(String location, Request request){
            this.location = location.replace("","");
            this.request = request;
        }

        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            cityTextView.setText("");
            weatherTextView.setText("");
            countryTextView.setText("");
        }

        protected String doInBackground(Void... urls) {

            try {
                URL url;
                if (request == Request.CURRENT){
                    url = new URL(API_URL_CURRENT + location +"&APPID="+API_KEY);
                }else{
                    url = new URL(API_URL_FORECAST + location +"&APPID="+API_KEY);
                }
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {

            if(response == null) {
                response = "THERE WAS AN ERROR";
            }
            try {
                JSONObject object = new JSONObject(response);
                Gson gson = new Gson();

                Reader reader = new StringReader(response);

                if(object.get("cod") != "404" ) {
                    fiveDayForecast = gson.fromJson(reader, FiveDayForecast.class);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressBar.setVisibility(View.GONE);
            Log.i("INFO", response);

            if (fiveDayForecast != null && fiveDayForecast.getCity() != null){
                Picasso.with(getContext())
                        .load(IMG_URL + fiveDayForecast.getListOfWeatherForcast().get(0).getWeather().get(0).getIcon() + ".png")
                        .centerCrop().resize(150,150)
                        .into(weatherImageView);
                cityTextView.setText(fiveDayForecast.getCity().getName());
                weatherTextView.setText("Weather: " + fiveDayForecast.getListOfWeatherForcast().get(0).getWeather().get(0).getDescription());
                countryTextView.setText(fiveDayForecast.getCity().getCountry());
            }
        }
    }
}